package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.Compte.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DepositServiceTest {
    @Mock
    DepositRepository depositRepository;
    @Mock
    CompteRepository compteRepository;
    @InjectMocks
    DepositService depositService;
    @Captor
    ArgumentCaptor<MoneyDeposit> moneyDepositArgumentCaptor;

    @Test
    @DisplayName("Should throw exception if destination account is not found")
    void shouldThrowExceptionWhenAccountNotFound() {
        //given
        DepositDto depositDto = new DepositDto("najim","prenom","123","motif",new BigDecimal(12),new Date());
        when(compteRepository.findByRib(Mockito.anyString())).thenReturn(Optional.empty());
        //then
        assertThrows(CompteNonExistantException.class,()-> depositService.createDeposit(depositDto));
    }

    @Test
    @DisplayName("should execute a deposit")
    void shouldExecuteDeposit(){
        //given
        DepositDto depositDto = new DepositDto("najim","prenom","123","motif",new BigDecimal(12),new Date());
        Compte compte = new Compte(1L,"1A","123",new BigDecimal(10),new Utilisateur());
        when(compteRepository.findByRib("123")).thenReturn(Optional.of(compte));
        //when
        depositService.createDeposit(depositDto);
        //then
        assertEquals(compte.getSolde(),new BigDecimal(22));
        verify(depositRepository,times(1)).save(moneyDepositArgumentCaptor.capture());
        assertEquals(moneyDepositArgumentCaptor.getValue().getMontant(),new BigDecimal(12));
        assertEquals(moneyDepositArgumentCaptor.getValue().getCompteBeneficiaire().getRib(),"123");

    }


}