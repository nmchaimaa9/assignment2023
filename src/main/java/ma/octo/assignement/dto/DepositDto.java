package ma.octo.assignement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DepositDto {
    public static final String MONTANT_MAXIMAL = "10000";
    @NotNull
    private String nomEmetteur;
    @NotNull
    private String prenomEmetteur;
    @NotEmpty
    private String ribCompteBeneficiaire;
    @NotEmpty
    private String motifDeposit;
    @NotNull
    @DecimalMax(value = MONTANT_MAXIMAL)
    private BigDecimal montant;
    private Date executionDate;
}
