package ma.octo.assignement.service;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;

import java.util.List;

public interface DepositServiceInterface {
    List<MoneyDeposit> getAllDeposits();
    void createDeposit(DepositDto depositDto);

}
