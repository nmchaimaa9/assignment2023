package ma.octo.assignement.exceptions.Compte;


import ma.octo.assignement.exceptions.GeneralException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class CompteNonExistantException extends GeneralException {

  private static final long serialVersionUID = 1L;

  public CompteNonExistantException() {
    super("CompteNonExistantException","Compte non existant exception");
  }

  public CompteNonExistantException(String nrCompte) {
    super("CompteNonExistantException",String.format("Le compte dont le numéro est %s est non existant ",nrCompte));
  }
}
