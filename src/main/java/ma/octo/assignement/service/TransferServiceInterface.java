package ma.octo.assignement.service;


import ma.octo.assignement.dto.TransferDto;

import java.util.List;

public interface TransferServiceInterface {
    List<TransferDto> getAllTransfers();
    void makeTransaction(TransferDto transferDto);
}
