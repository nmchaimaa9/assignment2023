package ma.octo.assignement.service;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class AccountService implements AccountServiceInterface{

    private final CompteRepository compteRepository;
    @Override
    public List<Compte> getAllAccounts(){
        return compteRepository.findAll();
    }
}
