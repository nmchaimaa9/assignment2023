package ma.octo.assignement.web;

import lombok.AllArgsConstructor;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.Compte.CompteNonExistantException;
import ma.octo.assignement.exceptions.Transfer.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.Transfer.TransactionException;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.TransferService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController(value = "/transfers")
@RequestMapping("/transfers")
@AllArgsConstructor
class TransferController {

    private final AuditService auditService;

    private final TransferService transferService;


    @GetMapping
    public List<TransferDto> loadAllTransfers() {
        return  transferService.getAllTransfers();

    }


    @PostMapping(path = "")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody @Valid TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        transferService.makeTransaction(transferDto);
        auditService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                .toString());
    }

}
