package ma.octo.assignement.exceptions.Transfer;

import ma.octo.assignement.exceptions.GeneralException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class SoldeDisponibleInsuffisantException extends GeneralException {

  private static final long serialVersionUID = 1L;

  public SoldeDisponibleInsuffisantException() {
    super("SoldeDisponibleInsuffisantException","Solde insuffisant ");
  }

  public SoldeDisponibleInsuffisantException(String nrCompte) {
    super("SoldeDisponibleInsuffisantException",String.format("Le solde du compte numéro %s est insuffisant",nrCompte));
  }
}
