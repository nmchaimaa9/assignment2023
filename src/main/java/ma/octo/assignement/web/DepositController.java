package ma.octo.assignement.web;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.Compte.CompteNonExistantException;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.DepositService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path="/deposits")
public class DepositController {

    private final DepositService depositService;
    private final AuditService auditService;

    @GetMapping("")
    public List<MoneyDeposit> loadAllDeposits(){
       return depositService.getAllDeposits();
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public void createDeposit(@RequestBody @Valid DepositDto depositDto)
            throws CompteNonExistantException{
        depositService.createDeposit(depositDto);
        auditService.auditDeposit("Deposit par " + depositDto.getNomEmetteur()+ depositDto.getPrenomEmetteur() + " au profit du compte de rib " + depositDto
                .getRibCompteBeneficiaire() + " d'un montant de " + depositDto.getMontant()
                .toString());
    }
}
