package ma.octo.assignement.service;

public interface AuditServiceInterface {
    void auditTransfer(String message);
    void auditDeposit(String message);
}
