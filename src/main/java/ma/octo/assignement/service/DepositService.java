package ma.octo.assignement.service;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.Compte.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class DepositService implements DepositServiceInterface {

    private final DepositRepository depositRepository;

    private final CompteRepository compteRepository;

    @Override
    public List<MoneyDeposit> getAllDeposits(){
        return depositRepository.findAll();
    }

    @Override
    public void createDeposit(DepositDto depositDto){

        Compte compteBeneficiaire = compteRepository.findByRib(depositDto.getRibCompteBeneficiaire())
                .orElseThrow(()-> new CompteNonExistantException(depositDto.getRibCompteBeneficiaire()) );

        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(depositDto.getMontant()));

        MoneyDeposit deposit = new MoneyDeposit();
        deposit.setMotifDeposit(depositDto.getMotifDeposit());
        deposit.setCompteBeneficiaire(compteBeneficiaire);
        deposit.setDateExecution(depositDto.getExecutionDate());
        deposit.setMontant(depositDto.getMontant());
        deposit.setNomEmetteur(depositDto.getNomEmetteur());
        deposit.setPrenomEmetteur(depositDto.getPrenomEmetteur());

        depositRepository.save(deposit);

    }


}
