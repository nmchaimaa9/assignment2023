package ma.octo.assignement.service;


import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class UserService implements UserServiceInterface {

    private final UtilisateurRepository utilisateurRepository;

    @Override
    public List<Utilisateur> getAllUsers(){
        return utilisateurRepository.findAll();
    }
}
