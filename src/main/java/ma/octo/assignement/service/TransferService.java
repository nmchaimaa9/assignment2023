package ma.octo.assignement.service;

import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.Compte.CompteNonExistantException;
import ma.octo.assignement.exceptions.Transfer.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.mapper.EntityToDtoMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class TransferService implements TransferServiceInterface{

    private final TransferRepository transferRepository;
    private final CompteRepository compteRepository;

    @Override
    public List<TransferDto> getAllTransfers(){
        return transferRepository.findAll().stream().map(EntityToDtoMapper::transferToTransferDto).collect(Collectors.toList());
    }

    @Override
    public void makeTransaction(TransferDto transferDto){
        Compte compteEmetteur = compteRepository.findByNrCompte(transferDto.getNrCompteEmetteur())
                .orElseThrow(()-> new CompteNonExistantException(transferDto.getNrCompteEmetteur()) );
        Compte compteBeneficiaire = compteRepository
                .findByNrCompte(transferDto.getNrCompteBeneficiaire())
                .orElseThrow(()-> new CompteNonExistantException(transferDto.getNrCompteBeneficiaire()));

        if(compteEmetteur.getSolde().compareTo(transferDto.getMontant())<0)
            throw new SoldeDisponibleInsuffisantException(compteEmetteur.getNrCompte());

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(transferDto.getMontant()));

        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(transferDto.getMontant()));

        Transfer transfer = new Transfer();
        transfer.setDateExecution(transferDto.getDate());
        transfer.setCompteBeneficiaire(compteBeneficiaire);
        transfer.setCompteEmetteur(compteEmetteur);
        transfer.setMontantTransfer(transferDto.getMontant());
        transfer.setMotifTransfer(transferDto.getMotif());

        transferRepository.save(transfer);



    }
}
