package ma.octo.assignement.exceptions.Transfer;

import ma.octo.assignement.exceptions.GeneralException;

public class TransactionException extends GeneralException {

  private static final long serialVersionUID = 1L;

  public TransactionException() {
    super("TransactionException", "Un problème est survenu lors de la transaction");
  }

  public TransactionException(String message) {
    super("TransactionException",message);
  }
}
