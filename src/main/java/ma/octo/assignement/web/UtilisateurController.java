package ma.octo.assignement.web;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path="/users")
public class UtilisateurController {
    private final UserService userService;

    @GetMapping
    public List<Utilisateur> LoadAllUsers(){
        return userService.getAllUsers();
    }
}
