package ma.octo.assignement.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "TRAN")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Transfer {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(precision = 16, scale = 2, nullable = false)
  private BigDecimal montantTransfer;

  @Temporal(TemporalType.TIMESTAMP)
  private Date dateExecution;

  @ManyToOne
  @JoinColumn(name="compte_emetteur_id")
  private Compte compteEmetteur;

  @ManyToOne
  @JoinColumn(name="compte_beneficiaire_id")
  private Compte compteBeneficiaire;

  @Column(length = 200)
  private String motifTransfer;


}
