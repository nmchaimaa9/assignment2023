package ma.octo.assignement.dto;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransferDto {
  public static final String MONTANT_MAXIMAL = "10000";
  public static final String MONTANT_MINIMAL = "10";
  @NotEmpty
  private String nrCompteEmetteur;
  @NotEmpty
  private String nrCompteBeneficiaire;
  @NotEmpty
  private String motif;
  @NotNull
  @DecimalMin(value = MONTANT_MINIMAL)
  @DecimalMax(value = MONTANT_MAXIMAL)
  private BigDecimal montant;
  private Date date;


}
